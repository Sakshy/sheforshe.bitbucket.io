# SheForShe
A web application for women safety, empowerment and career development

SheForShe was created by me in my first hackathon, GSB Hacks. This was conducted in August 2021. It was a 24 hour women centric hackathon where I girls from different parts of world met with each other.

*Purpose of the project
As the hackathon theme was 'Women Empowerment', I being a noob in Web Development created this complete website in 24 hours. 

The features of this website are:
1. It aims to motivate women 
2. It shows helpline numbers for more than 2 countries
3. It helps women to raise voice against cyber bullying
4. It shows a guide on how to take help when you are a victim
5. It offers volunteering opportunities
6. It has an awesome collection of videos and blogs to get inspiration from

Although there are 100s of organisations and instituitions working for women empowerment, but still somewhere in the world, women suffer badly because of men and women. 
SheforShe encourages all the genders to help each other. If a women cant help another women then its high time we should not expect peace in the world.

Tech Stack - HTML, CSS, Vanilla JS

Demo video: https://www.youtube.com/watch?v=Z_fqbmX0qHw

Devpost: https://devpost.com/software/sheforshe
